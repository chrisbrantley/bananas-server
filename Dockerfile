FROM 8squares/django-base:1.8
MAINTAINER Chris Brantley

COPY src/ /opt/bananas

run pip install -r /opt/bananas/requirements.txt

EXPOSE 80
WORKDIR /opt/bananas
CMD ["python", "run_production_server.py"]