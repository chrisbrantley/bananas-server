from gevent import monkey
monkey.patch_all()

import pymysql
pymysql.install_as_MySQLdb()

import os
import django

from gevent.wsgi import WSGIServer

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bananas.settings")
django.setup()

from django.core.handlers.wsgi import WSGIHandler as DjangoWSGIApp
application = DjangoWSGIApp()
server = WSGIServer(("0.0.0.0", 80), application)
print "Starting server"
server.serve_forever()
