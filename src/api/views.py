from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework import filters
from api.serializers import ListSerializer, ListItemSerializer
from data.models import List, ListItem


class ListViewSet(viewsets.ModelViewSet):
    queryset = List.objects.all()
    serializer_class = ListSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('user',)
    permission_classes = (IsAuthenticated,)
    
    @detail_route(methods=['post'])
    def clear(self, request, pk=None):
        ListItem.objects.filter(list_id=pk).delete()
        return Response('OK', status.HTTP_200_OK)
        
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class ListItemViewSet(viewsets.ModelViewSet):
    queryset = ListItem.objects.all()
    serializer_class = ListItemSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('user', 'list')
    permission_classes = (IsAuthenticated,)
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)