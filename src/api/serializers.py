from rest_framework import serializers
from data.models import List, ListItem


class ListSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.pk')
    
    class Meta:
        model = List
        fields = ('id', 'name', 'user', 'created_at', 'updated_at')


class ListItemSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.pk')
    
    class Meta:
        model = ListItem
        fields = ('id', 'name', 'list', 'is_checked', 'user', 'created_at', 'updated_at')
