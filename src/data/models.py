from django.db import models
from django.contrib.auth.models import User


class List(models.Model):
    name = models.CharField(max_length=128)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "lists"


class ListItem(models.Model):
    list = models.ForeignKey(List, related_name='items')
    user = models.ForeignKey(User)
    name = models.CharField(max_length=128)
    is_checked = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "list_items"
