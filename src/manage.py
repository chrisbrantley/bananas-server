#!/usr/bin/env python

from gevent import monkey
monkey.patch_all()

import pymysql
pymysql.install_as_MySQLdb()

import os
import sys

if __name__ == "__main__":

    from django.core.management import execute_from_command_line

    is_testing = 'test' in sys.argv

    if is_testing:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bananas.test_settings")
        from django.conf import settings
        import coverage
        cov = coverage.coverage(source=settings.PROJECT_APPS, omit=['*/tests/*', '*/tests.py'])
        cov.erase()
        cov.start()
    else:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bananas.settings")

    execute_from_command_line(sys.argv)

    if is_testing:
        cov.stop()
        cov.save()
        cov.report()